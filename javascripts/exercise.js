(function () {
    window.exercise = {

        events: [
            // View events
            'fullscreen', 'fullscreenchange', 'fullscreenerror', 'MozEnteredDomFullscreen', 'MozScrolledAreaChanged', 'resize', 'scroll', 'sizemodechange',

            // Touch events
            'MozEdgeUIGesture', 'MozMagnifyGesture', 'MozMagnifyGestureStart', 'MozMagnifyGestureUpdate', 'MozPressTapGesture', 'MozRotateGesture', 'MozRotateGestureStart', 'MozRotateGestureUpdate', 'MozSwipeGesture', 'MozTapGesture', 'MozTouchDown', 'MozTouchMove', 'MozTouchUp', 'touchcancel', 'touchend', 'touchenter', 'touchleave', 'touchmove', 'touchstart',

            // Input device events
            //'click', 'contextmenu', 'DOMMouseScroll', 'dblclick', 'gamepadconnected', 'gamepaddisconnected', 'keydown', 'keypress', 'keyup', 'MozGamepadButtonDown', 'MozGamepadButtonUp', 'mousedown', 'mouseenter', 'mouseleave', 'mousemove', 'mouseout', 'mouseover', 'mouseup', 'mousewheel', 'MozMousePixelScroll', 'pointerlockchange', 'pointerlockerror', 'wheel',

            // Drag events
            'drag', 'dragdrop', 'dragend', 'dragenter', 'dragexit', 'draggesture', 'dragleave', 'dragover', 'dragstart', 'drop',

            // Uncategorized events
            'beforeunload', 'localized', 'message', 'message', 'message', 'MozAfterPaint', 'moztimechange', 'open', 'show'
        ],


        addListeners: function() {

        },

        addNodes: function () {
            var i, divNode;
            for (i = 0; i < 100; i++) {
                divNode = document.createElement("div");
                divNode.innerText = i;
                document.body.appendChild(divNode);

                document.querySelector("#console").innerHTML = "<br>" + "Node " + i + " added." + document.querySelector("#console").innerHTML
            }
        },

        eventHandler: function (e) {
            console.log('scroll', e);
            document.querySelector("#console").innerHTML = "<br>event? " + e.type + document.querySelector("#console").innerHTML;
        },

        show: function () {
            var i;

            exercise.addNodes();

            exercise.addListeners();


        }
    };
}());


window
    .addEventListener("load", exercise.show);

